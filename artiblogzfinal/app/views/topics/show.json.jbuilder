json.current_user current_user, :id unless current_user.nil?
json.topic @topic, :title, :content, :created_at, :user_id
json.comment @topic.comments, :title, :content, :created_at, :user_id
