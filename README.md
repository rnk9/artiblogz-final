# artiblogz-final

##FRANCAIS :

##DESCRIPTION :
    
ArtiblogzFinal est un projet qui ma été attribué lors de mon stage en entreprise pour mon cursus de ma 1ére année de BTS Service Informatique aux Organisations, par mes tuteurs afin d'apprendre Ruby on Rails selon la maniere du Behavior Driven Developpment(Developpement dirige par des tests unitaires.).
Ce projet consiste en un blog auquel un utilisateur peut créer des Topics et les commenter.
Un utilisateur non authentifié pourra consulter tout les topics, commentaires et informations des autres utilisateurs, ceci dit, afin de pouvoir modifier ou supprimer quoi que ce soit, l'utilisateur doit en être le proprietaire et être authentifié.
Toute action sauf les vues possedent leurs propre Test Rspec.

Ce projet ma pris 6 semaines.

##CONTACT :

Si vous avez une quelconque question concernant le projet Artiblogz n'hésitez pas à me contacter par mail sur cette adresse :
e.soirot@gmail.com

##INSTALLATION :

Pré-requis :    Dernière version de Ruby
                Dernière version de Rails

Une fois le projet téléchargé, veuillez extraire le contenu de l'archive téléchargé.
Avec l'invité de commande spécifique à ruby accédez au chemin du dossier contenant le projet puis saisissez la commande suivante. "rails s".
Le serveur permettant d'utiliser l'applciation va automatiquement se lancer.
Utilisez ensuite votre navigateur web puis saisissez l'adresse suivante dans la barre de navigation.
"127.0.0.1:3000"
Vous devriez accéder directement à la page d'accueil de l'application.

##UTILISATION :

En tant qu'utilisateur non connecté vous aurez la possibilité de consulter les articles publiés par d'autres utilisateurs, ainsi que les commentaires associées à cet article. 
Un utilisateur pourra aussi consulter les profils des autres utilisateurs.
Afin de pouvoir publier un article ou un commentaire il va tout d'abord falloir créer un compte puis de vous authentifier.
Un utilisateur pourra ensuite modifier ou supprimer articles et commentaires mais uniquements ceux ayant été publié par ce dernier. 
Il en est de même pour le profil des utilisateurs, seul le propriétaire authentifié pourra le modifier.

##CONTRIBUTION :

1. Forkez le projet
2. Créez une nouvelle branche (git checkout -b my-new-feature)
3. Faites Commit de vos modifications (git commit -am 'Add some feature')
4. Pushez fvers la branche (git push origin my-new-feature)
5. Créez un nouveau Pull Request

Bonne utilisation d'Artiblogz !

- English :

ArtiblogzFinal is a project given by my tutors during my internship for learning Ruby on Rails the Behavior Driven Developpement way.
It consist of a blog where a user can create Topics, and make some comments to those topics.
A non authentified user can see every topics, comments, and user informations but, to create, edit or delete any topics or comments, he must be authentified and be the owner.
Any actions but Views have their own Rspec test.

This Project took me 6 weeks.


-日本語：

ArtiblogzFinalは、研修の時に、Ruby on RAilsのBDDを学ぶため、僕の教育係から、渡されたプロジェクトです。
ArtiblogsFinalとは、ブログです、ユーザーがトピックを作りそのトピックにコメントができます。
ログインしていないユーザーは、全てのトピック、コメント、それぞれのユーザーの情報が見れることが出来ます。
ただし、それぞれのエヂット、デリート、クリエイトは、ログインしてないと出来ません。
ビユー意外、全てのRspecあります。

このプルジェクトは、６週間かかりました。
