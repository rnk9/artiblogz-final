require 'rails_helper'

describe UsersController do
  render_views

  let(:json) { JSON.parse(response.body) }
  let(:user) { create(:user) }
  let(:invalid_id) { 500 }

  describe 'USER NOT LOGGED IN,' do
    describe 'GET #SHOW,' do
      context 'Render Show,' do
        it 'Renders show and Returns 200.' do
          get :show, id: user.id
          expect(response).to render_template 'show'
          expect(response.status).to eq 200
        end
      end

      context 'User Not Found,' do
        it 'Renders user_missing and Returns 404.' do
          get :show, id: :invalid_id
          expect(response).to render_template 'user_missing'
          expect(response.status).to eq 404
        end
      end
    end

    describe 'GET #NEW,' do
      it 'Renders :new and Returns 200.' do
        get :new
        expect(response).to render_template :new
        expect(response.status).to eq 200
      end
    end

    describe 'POST #create,' do
      context 'With Valid Attributes,' do
        context 'with valid attributes,' do
          before do
            post :create, user: attributes_for(:user)
          end

          it 'Saves user, Redirects to root_path and Returns 302.' do
            expect(response).to redirect_to root_path
            expect(response.status).to eq 302
          end

          it 'Json, Render :show and Return 201' do
            post :create, format: :json, user: attributes_for(:user2)
            expect(response).to redirect_to root_path
            expect(response.status).to eq 302
          end
        end
      end

      context 'With Invalid Attributes,' do
        context 'with invalid attributes,' do
          before do
            post :create, user: attributes_for(:invalid_user)
          end

          it 'Does not save user, Renders :new and Returns 200.' do
            expect(User.third).to be_nil
            expect(response).to render_template :new
            expect(response.status).to eq 200
          end

          it 'Json, Render user.errors and Return 422' do
            post :create, format: :json, user: attributes_for(:invalid_user)
            expect(response.body).to eq "{\"mail\":[\"is invalid\"],\"password\":[\"is too short (minimum is 4 characters)\"]}"
            expect(response.status).to eq 422
          end
        end
      end
    end

    describe 'PUT #UPDATE,' do
      describe 'put #update' do
        let(:user) { create(:initial_user) }

        context 'Valid Attributes,' do
          it 'Does not update user,' do
            put :update, id: user.id, user: attributes_for(:updated_user)
            user.reload
            expect(user.mail).not_to eq 'updated@mail.com'
            expect(user.password).not_to eq 'updatedpassword'
            expect(user.password_confirmation).not_to eq 'updatedpassword'
            expect(user.firstname).not_to eq 'updatedfirstname'
            expect(user.lastname).not_to eq 'updatedlastname'
          end

          it 'Redirects to login_path and Returns 302.' do
            put :update, id: user.id, user: attributes_for(:updated_user)
            expect(response).to redirect_to login_path
            expect(response.status).to eq 302
          end

          it 'Json, Redirect to login_path and Returns 302' do
            put :update, id: user.id, user: attributes_for(:updated_user)
            expect(response).to redirect_to login_path
            expect(response.status).to eq 302
          end
        end

        context 'Invalid Attributes,' do
          it 'Does not update user,' do
            put :update, id: user.id, user: attributes_for(:invalid_user)
            user.reload
            expect(user.mail).not_to eq 'invalid@mail'
            expect(user.password).not_to eq 'ip'
            expect(user.firstname).not_to eq 'invalidfirstnameisveryveryinvalidyesindeed'
            expect(user.lastname).not_to eq 'invalidlastnamesveryveryinvalidyesindeed'
          end

          it 'Redirects to login_path and Returns 302.' do
            put :update, id: user.id, user: attributes_for(:invalid_user)
            expect(response).to redirect_to login_path
            expect(response.status).to eq 302
          end

          it 'Json, Redirects to login_path and Return 302' do
            put :update, format: :json, id: user.id, user: attributes_for(:invalid_user)
            expect(response).to redirect_to login_path
            expect(response.status).to eq 302
          end
        end

        context 'User Not Found,' do
          it 'Renders user_missing and Returns 404.' do
            put :update, id: :invalid_id
            expect(response).to render_template 'user_missing'
            expect(response.status).to eq 404
          end
        end
      end
    end
  end

  describe 'AUTHORIZED USER lOGGED IN,' do
    describe 'authorized user logged in' do
      before do
        session[:user_id] = user.id
      end

      describe 'GET #SHOW,' do
        context 'Render Show,' do
          it 'Renders show and Returns 200' do
            get :show, id: user.id
            expect(response).to render_template 'show'
            expect(response.status).to eq 200
          end
        end

        context 'User Not Found,' do
          it 'Renders user_missing and Returns 404.' do
            get :show, id: :invalid_id
            expect(response).to render_template 'user_missing'
            expect(response.status).to eq 404
          end
        end
      end

      describe 'GET #NEW,' do
        it 'Redirects to topics_path and Returns 302.' do
          get :new
          expect(response).to redirect_to topics_path
          expect(response.status).to eq 302
        end
      end

      describe 'POST #CREATE,' do
        context 'With Valid Attributes,' do
          context 'with valid attributes,' do
            before do
              post :create, user: attributes_for(:user)
            end

            it 'Does not save user, Redirects to topics_path and Returns 302.' do
              expect(response).to redirect_to topics_path
              expect(response.status).to eq 302
            end

            it 'Json, Redirects to topics_path and Returns 302' do
              post :create, format: :json, user: attributes_for(:user)
              expect(response).to redirect_to topics_path
              expect(response.status).to eq 302
            end
          end
        end

        context 'With Invalid Attributes,' do
          context 'with invalid attributes,' do
            before do
              post :create, user: attributes_for(:invalid_user)
            end

            it 'Does not save user, Redirects to topics_path and Returns 302.' do
              expect(User.third).to be_nil
              expect(response).to redirect_to topics_path
              expect(response.status).to eq 302
            end

            it 'Json, Redirects to topics_path and Returns 302' do
              post :create, format: :json, user: attributes_for(:invalid_user)
              expect(response).to redirect_to topics_path
              expect(response.status).to eq 302
            end
          end
        end
      end

      describe 'PUT #UPDATE,' do
        describe 'put #update' do
          let(:user) { create(:initial_user) }

          context 'Valid Attributes,' do
            it 'Updates user,' do
              initialpassworddigest = user.password_digest
              put :update, id: user.id, user: attributes_for(:updated_user)
              user.reload
              expect(user.mail).to eq 'updated@mail.com'
              expect(user.password_digest).not_to eq initialpassworddigest
              expect(user.firstname).to eq 'updatedfirstname'
              expect(user.lastname).to eq 'updatedlastname'
            end

            it 'Redirects to user and Returns 302.' do
              put :update, id: user.id, user: attributes_for(:updated_user)
              expect(response).to redirect_to user
              expect(response.status).to eq 302
            end

            it 'Json, Render :show and Return 200' do
              put :update, format: :json, id: user.id, user: attributes_for(:updated_user)
              expect(response).to render_template :show
              expect(response.status).to eq 200
            end
          end

          context 'Invalid Attributes,' do
            it 'Does not update user attributes,' do
              put :update, id: user.id, user: attributes_for(:invalid_user)
              user.reload
              expect(user.mail).not_to eq 'invalid@mail'
              expect(user.password).not_to eq 'ip'
              expect(user.firstname).not_to eq 'invalidfirstnameisveryveryinvalidyesindeed'
              expect(user.lastname).not_to eq 'invalidlastnamesveryveryinvalidyesindeed'
            end

            it 'Renders :edit and Returns 200.' do
              put :update, id: user.id, user: attributes_for(:invalid_user)
              expect(response).to render_template :edit
              expect(response.status).to eq 200
            end

            it 'Json, Render user.errors and Return 422' do
              put :update, format: :json, id: user.id, user: attributes_for(:invalid_user)
              expect(response.body).to eq "{\"mail\":[\"is invalid\"],\"password\":[\"is too short (minimum is 4 characters)\"]}"
              expect(response.status).to eq 422
            end
          end

          context 'User Not Found,' do
            it 'Renders user_missing and Returns 404.' do
              put :update, id: :invalid_id
              expect(response).to render_template 'user_missing'
              expect(response.status).to eq 404
            end
          end
        end
      end
    end
  end

  describe 'UNAUTHORIZED USER lOGGED IN,' do
    describe 'unauthorized user logged in' do
      let(:unauthorizeduser) { create(:user2) }

      before do
        session[:user_id] = unauthorizeduser.id
      end

      describe 'GET #SHOW,' do
        context 'Render Show,' do
          it 'Renders show and Returns 200' do
            get :show, id: user.id
            expect(response).to render_template 'show'
            expect(response.status).to eq 200
          end
        end

        context 'User Not Found,' do
          it 'Renders user_missing and Returns 404.' do
            get :show, id: :invalid_id
            expect(response).to render_template 'user_missing'
            expect(response.status).to eq 404
          end
        end
      end

      describe 'GET #NEW,' do
        it 'Redirects to topics_path and Returns 302.' do
          get :new
          expect(response).to redirect_to topics_path
          expect(response.status).to eq 302
        end
      end

      describe 'POST #CREATE,' do
        context 'With Valid Attributes,' do
          context 'with valid attributes,' do
            before do
              post :create, user: attributes_for(:user)
            end

            it 'Does not save user, Redirects to topics_path and Returns 302.' do
              expect(response).to redirect_to topics_path
              expect(response.status).to eq 302
            end

            it 'Json, Redirects to topics_path and Return 302' do
              post :create, format: :json, user: attributes_for(:user)
              expect(response).to redirect_to topics_path
              expect(response.status).to eq 302
            end
          end
        end

        context 'With Invalid Attributes,' do
          context 'with invalid attributes,' do
            before do
              post :create, user: attributes_for(:invalid_user)
            end

            it 'Does not save user, Redirects to topics_path and Returns 302.' do
              expect(User.third).to be_nil
              expect(response).to redirect_to topics_path
              expect(response.status).to eq 302
            end

            it 'Json, Redirects to topics_path and Returns 302' do
              post :create, format: :json, user: attributes_for(:invalid_user)
              expect(response).to redirect_to topics_path
              expect(response.status).to eq 302
            end
          end
        end
      end

      describe 'PUT #UPDATE,' do
        describe 'put #update' do
          let(:user) { create(:initial_user) }

          context 'Valid Attributes,' do
            it 'Does not update user,' do
              put :update, id: user.id, user: attributes_for(:updated_user)
              user.reload
              expect(user.mail).not_to eq 'updated@mail.com'
              expect(user.password).not_to eq 'updatedpassword'
              expect(user.password_confirmation).not_to eq 'updatedpassword'
              expect(user.firstname).not_to eq 'updatedfirstname'
              expect(user.lastname).not_to eq 'updatedlastname'
            end

            it 'Redirects to topics_path and Returns 302.' do
              put :update, id: user.id, user: attributes_for(:updated_user)
              expect(response).to redirect_to topics_path
              expect(response.status).to eq 302
            end

            it 'Json, Redirects to topics_path and Return 302' do
              put :update, format: :json, id: user.id, user: attributes_for(:updated_user)
              expect(response).to redirect_to topics_path
              expect(response.status).to eq 302
            end
          end

          context 'Invalid Attributes,' do
            it 'Does not update user,' do
              put :update, id: user.id, user: attributes_for(:invalid_user)
              user.reload
              expect(user.mail).not_to eq 'invalid@mail'
              expect(user.password).not_to eq 'ip'
              expect(user.firstname).not_to eq 'invalidfirstnameisveryveryinvalidyesindeed'
              expect(user.lastname).not_to eq 'invalidlastnamesveryveryinvalidyesindeed'
            end

            it 'Redirects to topics_path and Returns 302.' do
              put :update, id: user.id, user: attributes_for(:invalid_user)
              expect(response).to redirect_to topics_path
              expect(response.status).to eq 302
            end

            it 'Json, Redirects to topics_path and Return 302.' do
              put :update, format: :json, id: user.id, user: attributes_for(:invalid_user)
              expect(response).to redirect_to topics_path
              expect(response.status).to eq 302
            end
          end

          context 'User Not Found,' do
            it 'Renders user_missing and Returns 404.' do
              put :update, id: :invalid_id
              expect(response).to render_template 'user_missing'
              expect(response.status).to eq 404
            end
          end
        end
      end
    end
  end
end
