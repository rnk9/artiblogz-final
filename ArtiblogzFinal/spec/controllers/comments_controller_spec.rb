require 'rails_helper'

describe CommentsController do
  render_views

  let(:json) { JSON.parse(response.body) }
  let(:invalid_id) { 500 }
  let(:user) { create(:user) }
  let(:topic) { create(:topic) }
  let(:comment) { topic.comments.create(title: 'Title', content: 'Content', user_id: user.id) }

  describe 'USER NOT LOGGED IN,' do
    describe 'POST #CREATE,' do
      context 'With Valid Attributes,' do
        before do
          post :create, topic_id: topic.id, comment: attributes_for(:comment)
          comment.user_id = user.id
        end

        it 'Redirects to login_path and Return 302.' do
          expect(response).to redirect_to login_path
          expect(response.status).to eq 302
        end

        it 'Json, Redirects to login_path and Return 302' do
          post :create, format: :json, topic_id: topic.id, comment: attributes_for(:comment)
          expect(response).to redirect_to login_path
          expect(response.status).to eq 302
        end
      end

      context 'With Invalid Attributes,' do
        context 'with invalid attributes,' do
          before do
            post :create, topic_id: topic.id, comment: attributes_for(:nul_comment)
          end

          it 'Does not save comment,' do
            expect(Comment.first).to be_nil
          end

          it 'Redirects to login_path and Return 302.' do
            expect(response).to redirect_to login_path
            expect(response.status).to eq 302
          end

          it 'Json, Redirects to login_path and Return 302' do
            post :create, format: :json, topic_id: topic.id, comment: attributes_for(:nul_comment)
            expect(response).to redirect_to login_path
            expect(response.status).to eq 302
          end
        end
      end

      context 'Topic Not Found,' do
        it 'Redirects to login_path and Return 302.' do
          post :create, topic_id: :invalid_id, comment: attributes_for(:comment)
          expect(response).to redirect_to login_path
          expect(response.status).to eq 302
        end
      end
    end

    describe 'PUT #UPDATE,' do
      context 'Valid Attributes,' do
        context 'valid attributes,' do
          before do
            put :update, topic_id: topic.id, id: comment.id, comment: attributes_for(:updated_comment)
          end

          it 'Does not update comment,' do
            comment.reload
            expect(comment.title).not_to eq 'Updated title'
            expect(comment.content).not_to eq 'Updated content'
          end

          it 'Redirects to login_path and Return 302.' do
            expect(response).to redirect_to login_path
            expect(response.status).to eq 302
          end

          it 'Json, Redirects to login_path and Return 302' do
            put :update, format: :json, topic_id: topic.id, id: comment.id, comment: attributes_for(:updated_comment)
            expect(response).to redirect_to login_path
            expect(response.status).to eq 302
          end
        end
      end

      context 'With Invalid Attributes,' do
        context 'with invalid attributes,' do
          before do
            put :update, topic_id: topic.id, id: comment.id, comment: attributes_for(:invalid_comment)
          end

          it 'Does not update comment,' do
            comment.reload
            expect(comment.title).not_to eq 'Invalid title'
            expect(comment.content).to eq 'Content'
          end

          it 'Redirects to login_path and Return 302.' do
            expect(response).to redirect_to login_path
            expect(response.status).to eq 302
          end

          it 'Json, Redirects to login_path and Returns 302' do
            put :update, format: :json, topic_id: topic.id, id: comment.id, comment: attributes_for(:invalid_comment)
            expect(response).to redirect_to login_path
            expect(response.status).to eq 302
          end
        end
      end

      context 'Topic Not Found,' do
        it 'Redirects to login_path and Return 302.' do
          put :update, topic_id: :invalid_id, id: comment.id, comment: attributes_for(:comment)
          expect(response).to redirect_to login_path
          expect(response.status).to eq 302
        end
      end

      context 'Comment Not Found,' do
        it 'Redirects to login_path and Return 302.' do
          put :update, topic_id: topic.id, id: :invalid_id, comment: attributes_for(:comment)
          expect(response).to redirect_to login_path
          expect(response.status).to eq 302
        end
      end
    end

    describe 'DELETE #DESTROY,' do
      context 'Do Not Delete Comment,' do
        before do
          delete :destroy, topic_id: topic.id, id: comment.id
        end

        it 'Redirects to login_path and Return 302.' do
          expect(response.status).to eq 302
          expect(response).to redirect_to login_path
        end
      end

      context 'Topic Not Found,' do
        it 'Redirects to login_path and Return 302.' do
          delete :destroy, topic_id: :invalid_id, id: comment.id, comment: attributes_for(:comment)
          expect(response).to redirect_to login_path
          expect(response.status).to eq 302
        end
      end

      context 'Comment Not Found,' do
        it 'Redirects to login_path and Return 302.' do
          delete :destroy, topic_id: topic.id, id: :invalid_id, comment: attributes_for(:comment)
          expect(response).to redirect_to login_path
        end
      end
    end
  end

  describe 'AUTHORIZED USER LOGGED IN,' do
    describe 'authorized user logged in' do
      before do
        session[:user_id] = user.id
      end

      describe 'POST #CREATE,' do
        context 'With Valid Attributes,' do
          before do
            post :create, topic_id: topic.id, comment: attributes_for(:comment)
          end

          it "Saves topic, Redirects to the comment's topic and Return 302." do
            expect(response).to redirect_to topic_path(topic.id)
            expect(response.status).to eq 302
          end

          it 'Json, Redirects to topic_path and return 302' do
            post :create, format: :json, topic_id: topic.id, comment: attributes_for(:comment)
            expect(response).to redirect_to topic_path(topic.id)
            expect(response.status).to eq 302
          end
        end

        context 'With Invalid Attributes,' do
          before do
            post :create, topic_id: topic.id, comment: attributes_for(:nul_comment)
          end

          it 'Does not save comment, Renders :new and Return 200.' do
            expect(Comment.first).to be_nil
            expect(response).to render_template :_form
            expect(response.status).to eq 200
          end

          it 'Json, Render comment.errors and Return 422' do
            post :create, format: :json, topic_id: topic.id, comment: attributes_for(:nul_comment)
            expect(response.body).to eq "{\"title\":[\"can't be blank\"],\"content\":[\"can't be blank\"]}"
            expect(response.status).to eq 422
          end
        end

        context 'Topic Not Found,' do
          it 'Renders topic_missing page and Return 404.' do
            post :create, topic_id: :invalid_id, comment: attributes_for(:comment)
            expect(response).to render_template 'topics/topic_missing'
            expect(response.status).to eq 404
          end
        end
      end

      describe 'PUT #UPDATE,' do
        describe 'put #update' do
          before do
            session[:user_id] = comment.user_id
          end

          context 'Valid Attributes,' do
            context 'valid attributes,' do
              before do
                put :update, topic_id: topic.id, id: comment.id, comment: attributes_for(:updated_comment)
              end

              it 'Updates comment,' do
                comment.reload
                expect(comment.title).to eq 'Updated title'
                expect(comment.content).to eq 'Updated content'
              end

              it 'Redirects to topic_path and Return 302.' do
                expect(response).to redirect_to topic_path
                expect(response.status).to eq 302
              end

              it 'Json, Render :show and Return 200' do
                put :update, format: :json, topic_id: topic.id, id: comment.id, comment: attributes_for(:updated_comment)
                expect(response).to redirect_to topic_path
                expect(response.status).to eq 302
              end
            end
          end

          context 'Invalid Attributes,' do
            context 'invalid attributes,' do
              before do
                put :update, topic_id: topic.id, id: comment.id, comment: attributes_for(:invalid_comment)
              end

              it 'Does not update comment,' do
                comment.reload
                expect(comment.title).not_to eq 'Invalid title'
                expect(comment.content).to eq 'Content'
              end

              it 'Renders edit and Return 200.' do
                expect(response).to render_template :edit
                expect(response.status).to eq 200
              end

              it 'Json, Render comment.errors and return 422' do
                put :update, format: :json, topic_id: topic.id, id: comment.id, comment: attributes_for(:invalid_comment)
                expect(response.body).to eq "{\"content\":[\"can't be blank\"]}"
                expect(response.status).to eq 422
              end
            end
          end

          context 'Topic Not Found,' do
            it 'Renders topic_missing page and Return 404.' do
              put :update, topic_id: :invalid_id, id: comment.id, comment: attributes_for(:comment)
              expect(response).to render_template 'topics/topic_missing'
              expect(response.status).to eq 404
            end
          end

          context 'Comment Not Found,' do
            it 'Renders comment_missing and Return 404.' do
              put :update, topic_id: topic.id, id: :invalid_id, comment: attributes_for(:comment)
              expect(response).to render_template :comment_missing
              expect(response.status).to eq 404
            end
          end
        end
      end

      describe 'DELETE #DESTROY,' do
        describe 'delete #destroy' do
          before do
            comment.user_id = user.id
            comment.reload
            session[:user_id] = user.id
          end

          context 'Delete Comment,' do
            it "Redirects to comment's topic and Return 302." do
              delete :destroy, topic_id: topic.id, id: comment.id
              expect(response).to redirect_to topic_path(topic.id)
              expect(response.status).to eq 302
            end

            it 'Json, Head no content' do
              delete :destroy, format: :json, topic_id: topic.id, id: comment.id
              expect(response.body).to be_empty
              expect(response.status).to eq 204
            end
          end

          context 'Topic Not Found,' do
            it 'Renders topic_missing and Return 404.' do
              delete :destroy, topic_id: :invalid_id, id: comment.id, comment: attributes_for(:comment)
              expect(response).to render_template 'topics/topic_missing'
              expect(response.status).to eq 404
            end
          end

          context 'Comment Not Found,' do
            it 'Renders comment_missing and Return 404.' do
              delete :destroy, topic_id: topic.id, id: :invalid_id, comment: attributes_for(:comment)
              expect(response).to render_template :comment_missing
              expect(response.status).to eq 404
            end
          end
        end
      end
    end
  end

  describe 'UNAUTHORIZED USER LOGGED IN,' do
    describe 'unauthorized user logged in,' do
      let(:unauthorizeduser) { create(:user2) }

      before do
        session[:user_id] = unauthorizeduser.id
      end

      describe 'POST #CREATE,' do
        context 'With Valid Attributes,' do
          before do
            post :create, topic_id: topic.id, comment: attributes_for(:comment)
          end

          it "Redirects to comment's topic and Return 302." do
            expect(response).to redirect_to topic_path(topic.id)
            expect(response.status).to eq 302
          end

          it 'Json, Redirects to topic_path(topic.id) and return 302' do
            post :create, format: :json, topic_id: topic.id, comment: attributes_for(:comment)
            expect(response).to redirect_to topic_path(topic.id)
            expect(response.status).to eq 302
          end
        end

        context 'With Invalid Attributes,' do
          context 'with invalid attributes,' do
            before do
              post :create, topic_id: topic.id, comment: attributes_for(:nul_comment)
            end

            it 'Does not save comment, Renders :_form and return 200.' do
              expect(Comment.first).to be_nil
              expect(response).to render_template :_form
              expect(response.status).to eq 200
            end

            it 'Json, Render comment.errors and Return 422' do
              post :create, format: :json, topic_id: topic.id, comment: attributes_for(:nul_comment)
              expect(response.body).to eq "{\"title\":[\"can't be blank\"],\"content\":[\"can't be blank\"]}"
              expect(response.status).to eq 422
            end
          end
        end

        context 'Topic Not Found,' do
          it 'Renders topic_missing and Return 404.' do
            post :create, topic_id: :invalid_id, comment: attributes_for(:comment)
            expect(response).to render_template 'topics/topic_missing'
            expect(response.status).to eq 404
          end
        end
      end

      describe 'PUT #UPDATE,' do
        context 'Valid Attributes,' do
          context 'valid attributes,' do
            before do
              put :update, topic_id: topic.id, id: comment.id, comment: attributes_for(:updated_comment)
            end

            it 'Does not update comment,' do
              comment.reload
              expect(comment.title).not_to eq 'Updated title'
              expect(comment.content).not_to eq 'Updated content'
            end

            it 'Redirects to topics_path and Return 302.' do
              expect(response).to redirect_to topics_path
              expect(response.status).to eq 302
            end

            it 'Json, Redirects to topics_path and return 302' do
              put :update, format: :json, topic_id: topic.id, id: comment.id, comment: attributes_for(:updated_comment)
              expect(response).to redirect_to topics_path
              expect(response.status).to eq 302
            end
          end
        end

        context 'Invalid Attributes,' do
          context 'invalid attributes,' do
            before do
              put :update, topic_id: topic.id, id: comment.id, comment: attributes_for(:invalid_comment)
            end

            it 'Does not update comment,' do
              comment.reload
              expect(comment.title).not_to eq 'Invalid title'
              expect(comment.content).to eq 'Content'
            end

            it 'Redirects to topics_path and Return 302.' do
              expect(response).to redirect_to topics_path
              expect(response.status).to eq 302
            end

            it 'Json, Redirects to topics_path and Return 302' do
              put :update, foramt: :json, topic_id: topic.id, id: comment.id, comment: attributes_for(:invalid_comment)
              expect(response).to redirect_to topics_path
              expect(response.status).to eq 302
            end
          end
        end

        context 'Topic Not Found,' do
          it 'Renders topic_missing and Return 404.' do
            put :update, topic_id: :invalid_id, id: comment.id, comment: attributes_for(:comment)
            expect(response).to render_template 'topics/topic_missing'
            expect(response.status).to eq 404
          end
        end

        context 'Comment Not Found,' do
          it 'Renders comment_missing and Return 404.' do
            put :update, topic_id: topic.id, id: :invalid_id, comment: attributes_for(:comment)
            expect(response).to render_template :comment_missing
            expect(response.status).to eq 404
          end
        end
      end

      describe 'DELETE #DESTROY,' do
        context 'Do not delete comment,' do
          it 'Redirects to topics_path and Return 302.' do
            delete :destroy, topic_id: topic.id, id: comment.id
            expect(response).to redirect_to topics_path
            expect(response.status).to eq 302
          end
        end

        context 'Topic Not Found,' do
          it 'Renders topic_missing and Return 404.' do
            delete :destroy, topic_id: :invalid_id, id: comment.id, comment: attributes_for(:comment)
            expect(response).to render_template 'topics/topic_missing'
            expect(response.status).to eq 404
          end
        end

        context 'Comment Not Found,' do
          it 'Renders comment_missing and Return 404.' do
            delete :destroy, topic_id: topic.id, id: :invalid_id, comment: attributes_for(:comment)
            expect(response).to render_template :comment_missing
            expect(response.status).to eq 404
          end
        end
      end
    end
  end
end
