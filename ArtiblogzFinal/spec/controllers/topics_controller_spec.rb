require 'rails_helper'

describe TopicsController do
  render_views

  let(:json) { JSON.parse(response.body) }
  let(:user) { create(:user) }
  let(:topic) { user.topics.create(title: 'Title', content: 'Content') }
  let(:invalid_id) { 500 }

  describe 'USER NOT LOGGED IN,' do
    describe 'GET #INDEX,' do
      describe 'get #index,' do
        before do
          get :index
        end

        it 'Renders index, and return 200' do
          expect(response).to render_template :index
          expect(response.status).to eq 200
        end

        # it 'Link header matchs topics and Returns 200.' do
        #   expect(response.headers['Link']).to match 'topics'
        #   expect(response.status).to eq 200
        # end
      end
    end

    describe 'GET #SHOW,' do
      describe 'get #show' do
        before do
          get :show, id: topic.id
        end

        context 'Get Show,' do
          it 'Renders :show and Returns 200.' do
            expect(response).to render_template :show
            expect(response.status).to eq 200
          end
        end

        context 'Topic Not Found,' do
          it 'Renders topic_missing and Returns 404.' do
            get :show, id: :invalid_id
            expect(response).to render_template 'topic_missing'
            expect(response.status).to eq 404
          end
        end
      end
    end

    describe 'GET #NEW,' do
      it 'Redirects to /login and Returns 302.' do
        get :new
        expect(response).to redirect_to '/login'
        expect(response.status).to eq 302
      end
    end

    describe 'POST #CREATE,' do
      context 'With Valid Attributes,' do
        context 'with valid attributes,' do
          before do
            post :create, topic: attributes_for(:topic)
          end

          it 'Redirects to /login and Returns 302.' do
            expect(response).to redirect_to '/login'
            expect(response.status).to eq 302
          end

          it 'Json, Redirects to /login and return 302' do
            post :create, format: :json, topic: attributes_for(:topic)
            expect(response).to redirect_to '/login'
            expect(response.status).to eq 302
          end
        end
      end

      context 'With Invalid Attributes,' do
        context 'with invalid attributes,' do
          before do
            post :create, topic: attributes_for(:invalid_topic)
          end

          it 'Does not save topic, Redirects to /login and Returns 302.' do
            expect(Topic.first).to be_nil
            expect(response).to redirect_to '/login'
            expect(response.status).to eq 302
          end

          it 'Json, Redirects to /login and Returns 302.' do
            post :create, format: :json, topic: attributes_for(:invalid_topic)
            expect(response).to redirect_to '/login'
            expect(response.status).to eq 302
          end
        end
      end
    end

    describe 'PUT #UPDATE,' do
      describe 'put #update,' do
        let(:topic) { create(:initial_topic) }

        context 'Valid Attributes,' do
          it 'Does not update topic,' do
            put :update, id: topic.id, topic: attributes_for(:updated_topic)
            topic.reload
            expect(topic.title).not_to eq 'Updated title'
            expect(topic.content).not_to eq 'Updated content'
          end

          it 'Redirects to /login and Returns 302,' do
            put :update, id: topic.id, topic: attributes_for(:topic)
            expect(response).to redirect_to '/login'
            expect(response.status).to eq 302
          end

          it 'Json, Redirects to /login and Returns 302' do
            put :update, format: :json, id: topic.id, topic: attributes_for(:topic)
            expect(response).to redirect_to '/login'
            expect(response.status).to redirect_to '/login'
          end
        end

        context 'Invalid Attributes,' do
          it 'Does not update topic,' do
            put :update, id: topic.id, topic: attributes_for(:invalid_topic)
            topic.reload
            expect(topic.title).not_to eq nil
            expect(topic.content).not_to eq 'Valid content'
          end

          it 'Redirects to /login and Returns 302.' do
            put :update, id: topic.id, topic: attributes_for(:invalid_topic)
            expect(response).to redirect_to '/login'
            expect(response.status).to eq 302
          end

          it 'Json, redirects to /login and Return 302' do
            put :update, id: topic.id, topic: attributes_for(:invalid_topic)
            expect(response).to redirect_to '/login'
            expect(response.status).to eq 302
          end
        end

        context 'Topic Not Found,' do
          it 'Redirects to /login and Returns 302.' do
            put :update, id: :invalid_id, topic: attributes_for(:invalid_topic)
            expect(response).to redirect_to '/login'
            expect(response.status).to eq 302
          end

          it 'Json, Redirects to /login and Return 302' do
            put :update, id: invalid_id, topic: attributes_for(:invalid_topic)
            expect(response).to redirect_to '/login'
            expect(response.status).to eq 302
          end
        end
      end
    end

    describe 'DELETE #DESTROY,' do
      context 'Delete Topic,' do
        it 'Redirects to /login and Returns 302.' do
          delete :destroy, id: topic.id
          expect(response).to redirect_to '/login'
          expect(response.status).to eq 302
        end
      end

      context 'Topic Not Found,' do
        it 'Renders topic_missing, and Returns 404.' do
          delete :destroy, id: :invalid_id
          expect(response).to redirect_to '/login'
          expect(response.status).to eq 302
        end
      end
    end
  end

  describe 'AUTHORIZED USER LOGGED IN,' do
    describe 'authorized user logged in' do
      before do
        session[:user_id] = user.id
      end

      describe 'GET #INDEX,' do
        describe 'get #index,' do
          before do
            get :index
          end

          it 'Renders :index,' do
            expect(response).to render_template :index
          end

          # it 'Link header matchs topics and Returns 200.' do
          #   expect(response.headers['Link']).to match 'topics'
          #   expect(response.status).to eq 200
          # end
        end
      end

      describe 'GET #SHOW,' do
        describe 'get #show,' do
          before do
            get :show, id: topic.id
          end

          it 'Renders show and Returns 200.' do
            expect(response).to render_template :show
            expect(response.status).to eq 200
          end
        end

        context 'Topic Not Found,' do
          it 'Renders topic_missing and Returns 404.' do
            get :show, id: :invalid_id
            expect(response).to render_template 'topic_missing'
            expect(response.status).to eq 404
          end
        end
      end

      describe 'GET #NEW,' do
        it 'Renders new and Returns 200.' do
          get :new
          expect(response.status).to eq 200
        end
      end

      describe 'POST #CREATE,' do
        context 'With Valid Attributes,' do
          context 'with valid attributes,' do
            before do
              post :create, topic: attributes_for(:topic)
            end

            it 'Saves topic, Redirects to Topic.first and Returns 302.' do
              expect(response).to redirect_to Topic.first
              expect(response.status).to eq 302
            end

            it 'Json, Render :show and Return 201' do
              post :create, format: :json, topic: attributes_for(:topic)
              expect(response).to render_template :show
              expect(response.status).to eq 201
            end
          end
        end

        context 'With Invalid Attributes,' do
          context 'with invalid attributes,' do
            before do
              post :create, topic: attributes_for(:invalid_topic)
            end

            it 'Does not save topic,　Renders :new　and Returns 200,' do
              expect(Topic.first).to be_nil
              expect(response).to render_template :new
              expect(response.status).to eq 200
            end

            it 'Json, Render :show and Return 201' do
              post :create, format: :json, topic: attributes_for(:invalid_topic)
              expect(response.body).to eq "{\"title\":[\"can't be blank\"],\"content\":[\"can't be blank\"]}"
              expect(response.status).to eq 422
            end
          end
        end
      end

      describe 'PUT #UPDATE,' do
        describe 'put #update,' do
          before do
            topic = create(:initial_topic, user_id: user.id)
            session[:user_id] = topic.user_id
          end

          context 'Valid Attributes,' do
            it 'Updates topic,' do
              put :update, id: topic.id, topic: attributes_for(:updated_topic)
              topic.reload
              expect(topic.title).to eq 'Updated title'
              expect(topic.content).to eq 'Updated content'
            end

            it 'Redirects to topic and Returns 302.' do
              put :update, id: topic.id, topic: attributes_for(:updated_topic)
              expect(response).to redirect_to topic
              expect(response.status).to eq 302
            end

            it 'Json, Render :show and Return 200' do
              put :update, format: :json, id: topic.id, topic: attributes_for(:updated_topic)
              expect(response).to render_template :show
              expect(response.status).to eq 200
            end
          end

          context 'Invalid Attributes,' do
            it 'Does not updates topic,' do
              put :update, id: topic.id, topic: attributes_for(:invalid_topic)
              topic.reload
              expect(topic.title).not_to eq nil
              expect(topic.content).not_to eq 'Valid content'
            end

            it 'Renders :edit and Returns 200.' do
              put :update, id: topic.id, topic: attributes_for(:invalid_topic)
              expect(response).to render_template :edit
              expect(response.status).to eq 200
            end

            it 'Json, Render json: topic.errors and Return 422' do
              put :update, format: :json, id: topic.id, topic: attributes_for(:invalid_topic)
              expect(response.body).to eq "{\"title\":[\"can't be blank\"],\"content\":[\"can't be blank\"]}"
              expect(response.status).to eq 422
            end
          end

          context 'Topic Not Found,' do
            it 'Renders topic_missing and Returns 404.' do
              put :update, id: :invalid_id, topic: attributes_for(:invalid_topic)
              expect(response).to render_template 'topic_missing'
              expect(response.status).to eq 404
            end
          end
        end
      end

      describe 'DELETE #destroy,' do
        context 'Delete Topic,' do
          it 'Redirects to topics#index and Returns 302.' do
            delete :destroy, id: topic.id
            expect(response).to redirect_to topics_path(topic.id)
            expect(response.status).to eq 302
          end

          it 'Json, head no content' do
            delete :destroy, format: :json, id: topic.id
            expect(response.body).to be_empty
            expect(response.status).to eq 204
          end
        end

        context 'Topic Not Found,' do
          it 'Renders topic_missing and Returns 404.' do
            delete :destroy, id: :invalid_id
            expect(response).to render_template 'topic_missing'
            expect(response.status).to eq 404
          end
        end
      end
    end
  end

  describe 'UNAUTHORIZED USER LOGGED IN,' do
    describe 'unauthorized user logged in' do
      let(:unauthorizeduser) { create(:user2) }

      before do
        session[:user_id] = unauthorizeduser.id
      end

      describe 'Get #INDEX,' do
        describe 'get #index,' do
          before do
            get :index
          end

          it 'Renders :index,' do
            expect(response).to render_template :index
          end

          # it 'Link header matchs topics and Returns 200.' do
          #   expect(response.headers['Link']).to match 'topics'
          #   expect(response.status).to eq 200
          # end
        end
      end

      describe 'GET #show,' do
        describe 'get #show,' do
          it 'Renders :show and Returns 200.' do
            get :show, id: topic.id
            expect(response).to render_template :show
            expect(response.status).to eq 200
          end
        end

        context 'Topic Not Found,' do
          it 'Renders topic_missing and Returns 404.' do
            get :show, id: :invalid_id
            expect(response).to render_template 'topic_missing'
            expect(response.status).to eq 404
          end
        end
      end

      describe 'GET #NEW,' do
        it 'Renders :new and Returns 200.' do
          get :new
          expect(response.status).to eq 200
        end
      end

      describe 'POST #CREATE,' do
        context 'With Valid Attributes,' do
          context 'with valid attributes,' do
            before do
              post :create, topic: attributes_for(:topic)
            end

            it 'Saves topic, Redirects to Topic.first and Returns 302.' do
              expect(response).to redirect_to Topic.first
              expect(response.status).to eq 302
            end

            it 'Json, Render :show and Return 201' do
              post :create, format: :json, topic: attributes_for(:topic)
              expect(response).to render_template :show
              expect(response.status).to eq 201
            end
          end
        end

        context 'With Invalid Attributes,' do
          context 'with invalid attributes,' do
            before do
              post :create, topic: attributes_for(:invalid_topic)
            end

            it 'Does not save topic, Renders :new and Returns 200,' do
              expect(Topic.first).to be_nil
              expect(response).to render_template :new
              expect(response.status).to eq 200
            end

            it 'Json, Render topic.errors and Return 422' do
              post :create, format: :json, topic: attributes_for(:invalid_topic)
              expect(response.body).to eq "{\"title\":[\"can't be blank\"],\"content\":[\"can't be blank\"]}"
              expect(response.status).to eq 422
            end
          end
        end
      end

      describe 'PUT #UPDATE,' do
        describe 'put #update' do
          let(:topic) { create(:initial_topic) }

          context 'Valid Attributes,' do
            it 'Does not update topic,' do
              put :update, id: topic.id, topic: attributes_for(:updated_topic)
              topic.reload
              expect(topic.title).not_to eq 'Updated title'
              expect(topic.content).not_to eq 'Updated content'
            end

            it 'Redirects to topics_path and Returns 302.' do
              put :update, id: topic.id, topic: attributes_for(:topic)
              expect(response).to redirect_to topics_path
              expect(response.status).to eq 302
            end

            it 'Json, Redirects to topics_path and Returns 302' do
              put :update, format: :json, id: topic.id, topic: attributes_for(:topic)
              expect(response).to redirect_to topics_path
              expect(response.status).to eq 302
            end
          end

          context 'Invalid Attributes,' do
            it 'Does not update topic,' do
              put :update, id: topic.id, topic: attributes_for(:invalid_topic)
              topic.reload
              expect(topic.title).not_to eq nil
              expect(topic.content).not_to eq 'Valid content'
            end

            it 'Redirects to topics_path and Returns 302.' do
              put :update, id: topic.id, topic: attributes_for(:invalid_topic)
              expect(response).to redirect_to topics_path
              expect(response.status).to eq 302
            end

            it 'Json, Redirects to topics_path and Return 302.' do
              put :update, format: :json, id: topic.id, topic: attributes_for(:invalid_topic)
              expect(response).to redirect_to topics_path
              expect(response.status).to eq 302
            end
          end

          context 'Topic Not Found,' do
            it 'Renders topic_missing and Returns 404.' do
              put :update, id: :invalid_id, topic: attributes_for(:invalid_topic)
              expect(response).to render_template 'topic_missing'
              expect(response.status).to eq 404
            end
          end
        end
      end

      describe 'DELETE #DESTROY,' do
        context 'Delete Topic,' do
          it 'Redirects to topics_path and Returns 302.' do
            delete :destroy, id: topic.id
            expect(response).to redirect_to topics_path
            expect(response.status).to eq 302
          end

          it 'Json, Redirects to topics_path and Returns 302.' do
              delete :destroy, format: :json, id: topic.id
              expect(response).to redirect_to topics_path
              expect(response.status).to eq 302
          end
        end

        context 'Topic Not Found,' do
          it 'Renders topic_missing and Returns 404.' do
            delete :destroy, id: :invalid_id
            expect(response).to render_template 'topic_missing'
            expect(response.status).to eq 404
          end
        end
      end
    end
  end
end
