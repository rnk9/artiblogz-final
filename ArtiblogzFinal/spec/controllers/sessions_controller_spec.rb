require 'rails_helper'

describe SessionsController do
  render_views

  let(:json) { JSON.parse(response.body) }
  let(:user) { create(:user) }

  describe 'GET #NEW' do
    it 'Renders :new and Returns 200' do
      get :new
      expect(response).to render_template :new
      expect(response.status).to eq 200
    end
  end

  describe 'POST #CREATE' do
    context 'Valid Attributes' do
      it 'Redirects to topics_path and Returns 302' do
        user.authenticate(:password)
        post :create, username: 'username', password: 'password'
        expect(response).to redirect_to topics_path
        expect(response.status).to eq 302
      end
    end

      it 'Json renders topics_path and return 201' do
        @user = user
        @user = User.find_by_username('username')
        @user.authenticate(username: 'username', password: 'password')
        post :create, format: :json, username: 'username', password: 'password'
        expect(response).to redirect_to topics_path
        expect(response.status).to eq 302
      end

    context 'Invalid Attributes' do
      it 'Redirects to login_path and Returns 302' do
        post :create
        expect(response).to redirect_to login_path
        expect(response.status).to eq 302
      end

      it 'Json render @session.error and return 422' do
        @user = user
        @user = User.find_by_username('username')
        @user.authenticate(username: 'WrongUser', password: 'password')
        post :create, format: :json, username: 'WrongUser', password: 'wrongpassword'
        expect(response.body).to eq 'Username or password invalid'
        expect(response.status).to eq 422
      end
    end
  end

  describe 'DELETE #DESTROY,' do
    context 'Delete session' do
      it 'Destroy session, Redirects to root_path and Return 302.' do
        delete :destroy
        expect(response).to redirect_to root_path
        expect(response.status).to eq 302
      end

      it 'Json no content' do
        delete :destroy, format: :json, id: 1
        expect(session[id: 1]).to eq nil
        expect(response.status).to eq 204
      end
    end
  end
end
