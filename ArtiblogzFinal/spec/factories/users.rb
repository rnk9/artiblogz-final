require 'faker'

FactoryGirl.define do
  factory :user do
    username 'username'
    mail Faker::Internet.email
    password 'password'
    password_confirmation 'password'
    firstname Faker::Name.first_name
    lastname Faker::Name.last_name

    factory :user2 do
      username 'username2'
      mail Faker::Internet.email
      password 'password'
      password_confirmation 'password'
      firstname Faker::Name.first_name
      lastname Faker::Name.last_name
    end

    factory :updated_user do
      mail 'updated@mail.com'
      password 'updatedpassword'
      password_confirmation 'updatedpassword'
      firstname 'updatedfirstname'
      lastname 'updatedlastname'
    end

    factory :initial_user do
      username 'initialusername'
      mail 'initial@mail.com'
      password 'initialpassword'
      password_confirmation 'initialpassword'
      firstname 'initialfirstname'
      lastname 'initiallastname'
    end

    factory :invalid_user do
      mail 'invalid@mail'
      password 'ip'
      password_confirmation 'ip'
      firstname 'invalidfirstnameisveryveryinvalidyesindeed'
      lastname 'invalidlastnamesveryveryinvalidyesindeed'
    end

    factory :username_too_long do
      username Faker::Lorem.characters(51)
      mail Faker::Internet.email
      password 'password'
      password_confirmation 'password'
      firstname Faker::Name.first_name
      lastname Faker::Name.last_name
    end

    factory :username_too_short do
      username Faker::Lorem.characters(3)
      mail Faker::Internet.email
      password 'password'
      password_confirmation 'password'
      firstname Faker::Name.first_name
      lastname Faker::Name.last_name
    end

    factory :username_missing do
      username nil
      mail Faker::Internet.email
      password 'password'
      password_confirmation 'password'
      firstname Faker::Name.first_name
      lastname Faker::Name.last_name
    end

    factory :username_not_unique do
      username 'username'
      mail Faker::Internet.email
      password 'password'
      password_confirmation 'password'
      firstname Faker::Name.first_name
      lastname Faker::Name.last_name
    end

    factory :mail_too_long do
      username Faker::Internet.user_name
      mail 'atoolongemailadressmaybeinserted@thiscanreallyreallyhappend.really'
      password 'password'
      password_confirmation 'password'
      firstname Faker::Name.first_name
      lastname Faker::Name.last_name
    end

    factory :mail_not_conform do
      username Faker::Internet.user_name
      mail 'mailadress@notconform'
      password 'password'
      password_confirmation 'password'
      firstname Faker::Name.first_name
      lastname Faker::Name.last_name
    end

    factory :mail_is_not_unique do
      username Faker::Internet.user_name
      mail 'adress@mail.com'
      password 'password'
      password_confirmation 'password'
      firstname Faker::Name.first_name
      lastname Faker::Name.last_name
    end

    factory :mail_missing do
      username Faker::Internet.user_name
      mail nil
      password 'password'
      password_confirmation 'password'
      firstname Faker::Name.first_name
      lastname Faker::Name.last_name
    end

    factory :password_too_short do
      username Faker::Internet.user_name
      mail Faker::Internet.email
      password 'ip'
      password_confirmation 'ip'
      firstname Faker::Name.first_name
      lastname Faker::Name.last_name
    end

    factory :password_too_long do
      username Faker::Internet.user_name
      mail Faker::Internet.email
      password '123456789012345678901234567890123456789012345678901'
      password_confirmation '123456789012345678901234567890123456789012345678901'
      firstname Faker::Name.first_name
      lastname Faker::Name.last_name
    end

    factory :password_missing do
      username Faker::Internet.user_name
      mail Faker::Internet.email
      password nil
      password_confirmation nil
      firstname Faker::Name.first_name
      lastname Faker::Name.last_name
    end

    factory :password_mismatch do
      username Faker::Internet.user_name
      mail Faker::Internet.email
      password 'password'
      password_confirmation 'wordssap'
      firstname Faker::Name.first_name
      lastname Faker::Name.last_name
    end

    factory :firstname_too_long do
      username Faker::Internet.user_name
      mail Faker::Internet.email
      password 'password'
      password_confirmation 'password'
      firstname Faker::Lorem.characters(51)
      lastname Faker::Name.last_name
    end

    factory :lastname_too_long do
      username Faker::Internet.user_name
      mail Faker::Internet.email
      password 'password'
      password_confirmation 'password'
      firstname Faker::Name.first_name
      lastname Faker::Lorem.characters(51)
    end
  end
end
