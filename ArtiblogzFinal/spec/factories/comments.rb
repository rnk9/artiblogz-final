require 'faker'

FactoryGirl.define do
  factory :comment do
    title Faker::Lorem.word
    content Faker::Lorem.sentence

    factory :invalid_comment do
      title 'Invalid Title'
      content nil
    end

    factory :nul_comment do
      title nil
      content nil
    end

    factory :updated_comment do
      title 'Updated title'
      content 'Updated content'
    end

    factory :initial_comment do
      title 'initial topic'
      content 'initial content'
    end
  end
end
