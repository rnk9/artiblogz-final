require 'faker'

FactoryGirl.define do
  factory :topic do
    title 'Topic'
    content 'Content'

    factory :topic_title_too_long do
      title SecureRandom.hex(26)
      content Faker::Lorem.sentence
    end

    factory :topic_content_too_long do
      title Faker::Lorem.word
      content SecureRandom.hex(76)
    end

    factory :topic_title_missing do
      title nil
      content Faker::Lorem.sentence
    end

    factory :topic_content_missing do
      title Faker::Lorem.word
      content nil
    end

    factory :invalid_topic do
      title nil
      content nil
    end

    factory :initial_topic do
      title 'Initial title'
      content 'Initial content'
    end

    factory :updated_topic do
      title 'Updated title'
      content 'Updated content'
    end
  end
end
