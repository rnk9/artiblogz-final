require 'rails_helper'

describe Comment do
  let(:user) { create(:user) }
  let(:topic) { user.topics.create(title: 'Title', content: 'Content') }

  context 'Comment creation' do
    it 'Succeed' do
      comment = topic.comments.create(title: 'Title',
                                      content: 'Content')
      expect(comment).to be_valid
    end

    it 'Comment title too long' do
      comment = topic.comments.create(title: SecureRandom.hex(26),
                                      content: 'Content')
      expect(comment).to be_invalid
    end

    it 'Comment content is too long' do
      comment = topic.comments.create(title: 'Title',
                                      content: SecureRandom.hex(76))
      expect(comment).to be_invalid
    end

    it 'Comment without title' do
      comment = topic.comments.create(title: nil, content: 'Content')
      expect(comment).to be_invalid
    end

    it 'Comment without content' do
      comment = topic.comments.create(title: 'Title', content: nil)
      expect(comment).to be_invalid
    end
  end

  context '2 or more comments' do
    it 'orders by newest comment' do
      comment1 = topic.comments.create(title: 'Title1',
                                       content: 'Content1')
      comment2 = topic.comments.create(title: 'Title2',
                                       content: 'Content2')
      expect(topic.reload.comments).to eq([comment2, comment1])
    end
  end

  context 'Has many Belongs to' do
    it 'has many topics' do
      reflection_topic = User.reflect_on_association(:user)

      if !reflection_topic.nil?
        if reflection_topic.macro == :has_many
          # everything is alright
        else
          # it's not has_many but exists
        end
      else
        # it doesn't exist at all !
      end
    end

    it 'has many comments' do
      reflection_comment = User.reflect_on_association(:user)

      if !reflection_comment.nil?
        if reflection_comment.macro == :has_many
          # everything is alright
        else
          # it's not has_many but exists
        end
      else
        # it doesn't exist at all !
      end
    end

    it 'belongs to user' do
      reflection_user = Topic.reflect_on_association(:topic)

      if !reflection_user.nil?
        if reflection_user.macro == :belongs_to
          # everything is alright!
        else
          # it's not belongs_to but exists
        end
      else
        # it doesn't exist at all!
      end
    end

    it 'belongs to user' do
      reflection_user = Comment.reflect_on_association(:comment)

      if !reflection_user.nil?
        if reflection_user.macro == :belongs_to
          # everything is alright!
        else
          # it's not belongs_to but exists
        end
      else
        # it doesn't exist at all!
      end
    end
  end
end
