require 'rails_helper'

describe Topic do
  let(:user) { create(:user) }

  context 'Topic creation success' do
    it { expect(create(:topic)).to be_valid }
  end

  context 'Topic title too long' do
    it { expect(build(:topic_title_too_long)).to be_invalid }
  end

  context 'Topic content is too long' do
    it { expect(build(:topic_content_too_long)).to be_invalid }
  end

  context 'Topic without title' do
    it { expect(build(:topic_title_missing)).to be_invalid }
  end

  context 'Topic without content' do
    it { expect(build(:topic_content_missing)).to be_invalid }
  end

  context 'With 2 or more topics' do
    it 'Orders by newest topic' do
      topic1 = create(:topic)
      topic2 = create(:topic)
      topic3 = create(:topic)
      expect(Topic.order('created_at DESC')).to eq([topic3, topic2, topic1])
    end
  end

  context 'Topic with comments' do
    it 'orders by newest comment' do
      topic = create(:topic)
      comment1 = topic.comments.create(title: 'Title1',
                                       content: 'Content1')
      comment2 = topic.comments.create(title: 'Title2',
                                       content: 'Content2')
      expect(topic.reload.comments).to eq([comment2, comment1])
    end
  end

  context 'Topic comment delete dependency' do
    before do
      @topic = Topic.create
      @comments = [Comment.create]
      @topic.comments = @comments
    end

    it 'should destroy all of the associated comments' do
      @comments.each { |s| expect(Comment.exists?(s.id)).to eq(false) }
    end
  end

  context 'Has many Belongs to' do
    it 'has many' do
      reflection_comment = Topic.reflect_on_association(:topic)

      if !reflection_comment.nil?
        if reflection_comment.macro == :has_many
          # everything is alright
        else
          # it's not has_many but exists
        end
      else
        # it doesn't exist at all !
      end
    end

    it 'belongs to' do
      reflection_topic  = Comment.reflect_on_association(:comment)

      if !reflection_topic.nil?
        if reflection_topic.macro == :belongs_to
          # everything is alright!
        else
          # it's not belongs_to but exists
        end
      else
        # it doesn't exist at all!
      end
    end
  end
end
