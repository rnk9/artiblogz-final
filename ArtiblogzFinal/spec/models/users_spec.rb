require 'rails_helper'
require 'factory_girl'

describe User do
  let(:user) { create(:user) }

  context 'User creation success' do
    it { expect(create(:user)).to be_valid }
  end

  context 'Username too long' do
    it { expect(build(:username_too_long)).to be_invalid }
  end

  context 'Username too short' do
    it { expect(build(:username_too_short)).to be_invalid }
  end

  context 'Username missing' do
    it { expect(build(:username_missing)).to be_invalid }
  end

  context 'Username Is Not Unique' do
    it 'username is not unique'do
      create(:username_not_unique)
      expect(build(:username_not_unique)).to be_invalid
    end
  end

  context 'User mail too long' do
    it { expect(build(:mail_too_long)).to be_invalid }
  end

  context 'User mail not conform' do
    it { expect(build(:mail_not_conform)).to be_invalid }
  end

  context 'User Mail Is Not Unique' do
    it 'user mail is not unique'do
      create(:mail_is_not_unique)
      expect(build(:mail_is_not_unique)).to be_invalid
    end
  end

  context 'User mail is mising' do
    it { expect(build(:mail_missing)).to be_invalid }
  end

  context 'Password too short' do
    it { expect(build(:password_too_short)).to be_invalid }
  end

  context 'Password too long' do
    it { expect(build(:password_too_long)).to be_invalid }
  end

  context 'Password missing' do
    it { expect(build(:password_missing)).to be_invalid }
  end

  context 'Password mismatch' do
    it { expect(build(:password_mismatch)).to be_invalid }
  end

  context 'User firstname too long' do
    it { expect(build(:firstname_too_long)).to be_invalid }
  end

  context 'User lastname too long' do
    it { expect(build(:lastname_too_long)).to be_invalid }
  end

  context 'Has many Belongs to' do
    it 'has many topics' do
      reflection_topic = User.reflect_on_association(:user)

      if !reflection_topic.nil?
        if reflection_topic.macro == :has_many
          # everything is alright
        else
          # it's not has_many but exists
        end
      else
        # it doesn't exist at all !
      end
    end

    it 'has many comments' do
      reflection_comment = User.reflect_on_association(:user)

      if !reflection_comment.nil?
        if reflection_comment.macro == :has_many
          # everything is alright
        else
          # it's not has_many but exists
        end
      else
        # it doesn't exist at all !
      end
    end

    it 'belongs to' do
      reflection_user = Topic.reflect_on_association(:topic)

      if !reflection_user.nil?
        if reflection_user.macro == :belongs_to
          # everything is alright!
        else
          # it's not belongs_to but exists
        end
      else
        # it doesn't exist at all!
      end
    end

    it 'belongs to' do
      reflection_user = Comment.reflect_on_association(:comment)

      if !reflection_user.nil?
        if reflection_user.macro == :belongs_to
          # everything is alright!
        else
          # it's not belongs_to but exists
        end
      else
        # it doesn't exist at all!
      end
    end
  end
end
