class User < ActiveRecord::Migration
  def change
    create_table :topics do |t|
      t.string :title
      t.text :content
      t.string :username
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end

    create_table :comments do |t|
      t.string :title
      t.text :content
      t.string :username
      t.references :topic, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end

    create_table :users do |t|
      t.string :username, null: false
      t.string :mail, null: false
      t.string :firstname
      t.string :lastname
      t.string :password_digest, null: false

      t.timestamps null: false
    end
  end
end
