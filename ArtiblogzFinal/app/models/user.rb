class User < ActiveRecord::Base
  before_save { self.mail = mail.downcase }

  has_many :comments, dependent: :destroy
  has_many :topics, dependent: :destroy

  validates :username, presence: true,
                       length: { in: 4..50 },
                       uniqueness: { case_sensitive: false }

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]{2,4}+\z/i

  validates :mail, presence: true,
                   length: { maximum: 50 },
                   format: { with: VALID_EMAIL_REGEX },
                   uniqueness: { case_sensitive: false }

  validates :firstname, length: { maximum: 50 }

  validates :lastname, length: { maximum: 50 }

  has_secure_password
  validates :password, confirmation: true
  validates :password_confirmation, presence: true
  validates_length_of :password, in: 4..50
end
