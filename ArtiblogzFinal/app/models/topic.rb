class Topic < ActiveRecord::Base
  default_scope { order(created_at: :DESC) }

  has_many :comments, dependent: :destroy
  belongs_to :user
  validates :title, presence: true,
                    length: { maximum: 50 }
  validates :content, presence: true,
                      length: { maximum: 140 }
end
