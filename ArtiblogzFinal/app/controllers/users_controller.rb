class UsersController < ApplicationController
  # force_ssl

  before_action :set_user, only: [:show, :edit, :update, :authorization]
  before_filter :authorize, only: [:edit, :update]
  before_action :authorization, only: [:edit, :update]
  before_action :uselessaction, only: [:new, :create]

  def show
    # response.headers['Link'] = 'user'
    fail ActiveRecord::RecordNotFound if @user.nil?
  end

  def new
    # response.headers['Link'] = 'new_user'
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    respond_to do |format|
      if @user.save
        format.html { redirect_to root_path, notice: 'Account created ! Welcome to ArtiBlogz.' }
        format.json { redirect_to root_path, status: 302, location: @user }
      else
        format.html { render :new, notice: 'Invalid attributes' }
        format.json { render json: @user.errors, status: 422 }
      end
    end
  end

  def edit
    # response.headers['Link'] = 'edit_user'
  end

  def update
    respond_to do |format|
      if @user.nil?
        fail ActiveRecord::RecordNotFound
      elsif @user.update(user_params)
        format.html { redirect_to @user, notice: 'Account updated.' }
        format.json { render :show, status: 200, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: 422 }
      end
    end
  end

  rescue_from ActiveRecord::RecordNotFound do
    render action: 'user_missing', status: 404
  end

  private

  def user_params
    params.require(:user).permit(:username,
                                 :mail,
                                 :password,
                                 :password_confirmation,
                                 :firstname,
                                 :lastname)
  end

  def authorization
    redirect_to topics_path, notice: "You can't do that !" unless current_user.id == @user.id
  end

  def uselessaction
    redirect_to topics_path, notice: 'Log off to create a new account' unless current_user.nil?
  end

  def set_user
    @user = User.find(params[:id])
  end
end
