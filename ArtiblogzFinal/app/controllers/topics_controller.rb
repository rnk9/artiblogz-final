class TopicsController < ApplicationController
  # force_ssl

  before_filter :authorize, only: [:new, :create, :edit, :update, :destroy]
  before_action :set_topic, only: [:show, :edit, :update, :destroy, :authorization]
  before_action :authorization, only: [:edit, :update, :destroy]

  def index
    # response.headers['Link'] = 'topics'
    @topics = Topic.all
  end

  def show
    # response.headers['Link'] = 'topic'
  end

  def new
    # response.headers['Link'] = 'new_topic'
    @topic = Topic.new
  end

  def create
    @topic = Topic.new(topic_params)
    @topic.user_id = current_user.id
    @user = User.find(current_user.id)
    @topic.username = @user.username
    respond_to do |format|
      if @topic.save
        format.html { redirect_to @topic, notice: 'Topic created.' }
        format.json { render :show, status: 201, location: @topic }
      else
        format.html { render :new }
        format.json { render json: @topic.errors, status: 422 }
      end
    end
  end

  def edit
    # response.headers['Link'] = 'edit_topic'
  end

  def update
    respond_to do |format|
      if @topic.nil?
        fail ActiveRecord::RecordNotFound
      elsif @topic.update(topic_params)
        format.html { redirect_to @topic, notice: 'Topic updated.' }
        format.json { render :show, status: 200, location: @topic }
      else
        format.html { render :edit }
        format.json { render json: @topic.errors, status: 422 }
      end
    end
  end

  def destroy
    if @topic.nil?
      fail ActiveRecord::RecordNotFound
    else
      @topic.destroy
      respond_to do |format|
        format.html { redirect_to topics_path(@topic), notice: 'Topic deleted.' }
        format.json { head :no_content, status: 204 }
      end
    end
  end

  rescue_from ActiveRecord::RecordNotFound do
    render :topic_missing, status: 404
  end

  private

  def topic_params
    params.require(:topic).permit(:title, :content) if params[:topic]
  end

  def set_topic
    @topic = Topic.find(params[:id])
  end

  def authorization
    redirect_to topics_path, notice: "You can't do that !" unless current_user.id == @topic.user_id
  end
end
