class SessionsController < ApplicationController
	# force_ssl

	def new
		# response.headers['Link'] = 'login'
	end

	def create
		@user = User.find_by_username(params[:username])
		# If the user exists AND the password entered is correct
		respond_to do |format|
			if @user && @user.authenticate(params[:password])
				# Save the user id inside the browser cookie, keep the user logged in
				# when they navigate around the website.
				session[:user_id] = @user.id
				# @session = @user.id
				format.html { redirect_to topics_path, notice: 'Welcome Back !' }
				format.json { redirect_to topics_path, status: 302, notice: 'Connection Success.' }
			else
				format.html { redirect_to login_path, notice: 'Username or password is invalid.' }
				# json: session.errors ?
				format.json { render json: 'Username or password invalid', status: 422 }
				# render json: @session.errors,
			end
		end
	end

  def destroy
		# response.headers['Link'] = 'logout'
		respond_to do |format|
			session[:user_id] = nil
    	format.html { redirect_to root_path, notice: 'Successfully logged out.' }
			format.json { head :no_content, status: 204 }
		end
  end
end
