class CommentsController < ApplicationController
  # force_ssl

  before_action :find_comment, only: [:edit, :authorization]
  before_filter :authorize, only: [:new, :create, :edit, :update, :destroy]
  before_action :rescuetag, only: [:update, :destroy]
  before_action :set_topic, only: [:create, :show, :edit, :update, :destroy, :authorization]
  before_action :find_topic, only: [:update, :destroy, :authorization]
  before_action :authorization, only: [:edit, :update, :destroy]

  def create
    @comment = @topic.comments.create(comment_params)
    @comment.user_id = current_user.id
    @user = User.find(current_user.id)
    @comment.username = @user.username
    respond_to do |format|
      if @topic.nil?
        rescuetag
        fail ActiveRecord::RecordNotFound
      elsif @comment.save
        format.html { redirect_to topic_path(@topic), notice: 'Comment created.' }
        format.json { redirect_to topic_path(@topic), status: 302, location: @comment }
      else
        format.html { render :_form, notice: 'Invalid attributes' }
        format.json { render json: @comment.errors, status: 422 }
      end
    end
  end

  def edit
    # response.headers['Link'] = 'edit_topic_comment'
  end

  def update
    if @topic.nil?
      rescuetag
      fail ActiveRecord::RecordNotFound
    elsif @comment.nil?
      rescuetag
      fail ActiveRecord::RecordNotFound
    elsif @comment.update(comment_params)
      respond_to do |format|
        format.html { redirect_to topic_path(@topic), notice: 'Comment updated.' }
        format.json { redirect_to topic_path(@topic), status: 302 }
      end
    else
      respond_to do |format|
        format.html { render :edit }
        format.json { render json: @comment.errors, status: 422 }
      end
    end
  end

  def destroy
    if @topic.nil?
      rescuetag
      fail ActiveRecord::RecordNotFound
    elsif @comment.nil?
      rescuetag
      fail ActiveRecord::RecordNotFound
    else
      @comment.destroy
      respond_to do |format|
        format.html { redirect_to topic_path(@topic), notice: 'Comment deleted.' }
        format.json { head :no_content, status: 204 }
      end
    end
  end

  rescue_from ActiveRecord::RecordNotFound do
    if rescuetag == 'topic'
      render 'topics/topic_missing', status: 404
    elsif rescuetag == 'content'
      render :comment_missing, status: 404
    end
  end

  private

  def rescuetag
    if @topic.nil?
      rescuetag = 'topic'
    elsif @comment.nil?
      rescuetag = 'content'
    end
  end

  def find_topic
    @comment = @topic.comments.find(params[:id])
  end

  def comment_params
    params.require(:comment).permit(:title, :content)
  end

  def set_topic
    @topic = Topic.find(params[:topic_id])
  end

  def find_comment
    @comment = Comment.find(params[:id])
  end

  def authorization
    redirect_to topics_path, notice: "You can't do that !" unless current_user.id == @comment.user_id
  end
end
